import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { Container,Row } from 'react-bootstrap';
import MyCard from './components/MyCard';
import MyTable from './components/MyTable';


export default class App extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            foods:[
                {
                    id: 1,
                    name: "Tiger",
                    thumbnail: "/images/tiger.jpg",
                    qty:0,
                    price:2,
                    total:0,
                },
                {
                    id: 2,
                    name: "Noodle",
                    thumbnail: "/images/Chicken-Noodle-Soup.jpg",
                    qty:0,
                    price:5,
                    total:0,
                },
                {
                    id: 3,
                    name: "Redbull",
                    thumbnail: "/images/redbull.jpg",
                    qty:0,
                    price:1,
                    total:0,
                },
                {
                    id: 4,
                    name: "Cheese Stack",
                    thumbnail: "/images/stack-cheese.jpg",
                    qty:0,
                    price:15,
                    total:0,
                }
            ]
        }
    }


    onAdd = (index)=>{
        console.log("Index: ", index);
        let temp = [...this.state.foods]
        temp[index].qty++
        
        temp[index].total = temp[index].qty*temp[index].price
        this.setState({
            foods: temp
        })
    }
    onDelete = (index)=>{
        console.log("Index: ", index);
        let temp = [...this.state.foods]
        temp[index].qty--

        temp[index].total = temp[index].qty*temp[index].price
        this.setState({
            foods: temp
        })
    }

    onClear = ()=>{
        let temp = [...this.state.foods]
        temp.map(item=>{
            item.qty =0
            item.price = 0
        })

        this.setState(
            {foods:temp}
        )
    }

    render(){
        return(
            <Container>
            <Row>
               <MyCard foods = {this.state.foods} onAdd={this.onAdd} onDelete={this.onDelete}/>
            </Row>
            <Row>
                <MyTable foods = {this.state.foods} onClear={this.onClear} />
            </Row>
            </Container>
        )
    }
}
