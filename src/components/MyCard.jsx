import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Card, Button, Col} from 'react-bootstrap';

export default class MyCard extends Component {
    render() {


      console.log("Foods Props: ", this.props.foods);


        return (
           <>
              {
                
              this.props.foods.map((item, index)=>(
                <Col xs="3" key={index}>
                    <Card>
                      <Card.Img variant="top" src={item.thumbnail} />
                      <Card.Body>
                        <Card.Title>{item.name}</Card.Title>
                          <Card.Text>
                            Price :  {item.price}
                          </Card.Text>
                      <Button variant="warning">{item.qty}</Button>{'  '}    
                      <Button disabled={item.qty ===0?true:false} onClick={()=>this.props.onDelete(index)} variant="danger">Delete</Button>{'  '}
                      <Button onClick={()=>this.props.onAdd(index)} variant="primary">Add</Button>{'  '}
                      
                      <h3>Total : {item.total} $</h3>
                    </Card.Body>
                  </Card>
                </Col>
              ))
             
             }
           </>
        )
    }
}
