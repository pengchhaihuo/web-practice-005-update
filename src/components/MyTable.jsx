import React from 'react'
import {Table,Button,Badge} from 'react-bootstrap'

export default function MyTable(props) {

    let temp = props.foods.filter(
        item=>{ return item.qty>0; }
    )
   
    console.log(temp);

    let Subtotal = 0;

    for( var i=0; i<temp.length;i++){
        Subtotal = Subtotal + (temp[i].price*temp[i].qty)
    }


    let Discount = Subtotal*0.2;

    let Total = Subtotal-Discount;

    console.log(Subtotal);

    return (
        <>
            <Button onClick={props.onClear} variant="info">Reset</Button>{'  '}
            <h6><Badge variant="warning">  {temp.length }{' count'} </Badge> </h6>
            <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Foods</th>
                    <th>Amount</th>
                    <th>Price</th>
                    <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        temp.map((item,index)=>(
                            <tr>
                            <td>{item.id}</td>
                            <td>{item.name}</td>
                            <td>{item.qty}</td>
                            <td>{item.price}$</td>
                            <td>{item.total}$</td>
                            </tr>
                        ))
                    }
                    <tr>
                    <td colSpan={4}>Subtotal</td>
                    <td>{Subtotal}$</td>
                    </tr>
                    <tr>
                    <td colSpan={4}>Discount</td>
                    <td>{Discount}$</td>
                    </tr>
                    <tr>
                    <td colSpan={4}>Total</td>
                    <td>{Total}$</td>
                    </tr>
                </tbody>
            </Table>
        </>
    )
}
